/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "i2c.h"
#include "usb_host.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "usbh_cdc.h"
#include "ssd1306_oled.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
uint8_t str_tx[32];
uint8_t i = 0;
uint8_t ii, jj;

volatile uint8_t str_rx[32];
volatile uint8_t err_flag = 0;
uint8_t block_counter = 0;
uint8_t text_block[5][24];
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void MX_USB_HOST_Process(void);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

extern ApplicationTypeDef Appli_state;
extern USBH_HandleTypeDef hUsbHostFS;

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
    if(Appli_state == APPLICATION_READY){
        // We can send the message only if the device is ready
        sprintf((char*)str_tx, "Host: Msg No. %d\r\n", i++);
        USBH_CDC_Transmit(&hUsbHostFS, str_tx, strlen((char*)str_tx));
        USBH_CDC_Receive(&hUsbHostFS, (uint8_t*)str_rx, 32);

    }else{
        // There is no device to send message to
        err_flag = 1;
    }
}

void USBH_CDC_ReceiveCallback(USBH_HandleTypeDef *phost){
    // Yay, we have a response from our loved slave, ... I mean device
        //There is data in the receiver buffer
    if(block_counter < 4){
        // Only add the new data
        for(ii = 0; ii < 24; ++ii){
            if(str_rx[ii] == '\r')
                break;
            text_block[block_counter][ii] = str_rx[ii];
        }
        for(jj = ii; jj < 24; ++jj)
            text_block[block_counter][ii] = 0;
        ++block_counter;
    }else{
        // Shift old data and add new
        for(uint8_t blk = 0; blk < 3; ++blk)
            for(ii = 0; ii < 24; ++ii){
                text_block[blk][ii] = text_block[blk+1][ii];
            }

        for(ii = 0; ii < 24; ++ii){
            if(str_rx[ii] == '\r')
                break;
            text_block[4][ii] = str_rx[ii];
        }
        for(jj = ii; jj < 24; ++jj)
            text_block[4][ii] = 0;
    }

    SSD1306_GotoXY(0, 0);
    SSD1306_Puts((char*)text_block[0], &font_7x10, SSD1306_COLOR_WHITE);
    SSD1306_GotoXY(0, 11);
    SSD1306_Puts((char*)text_block[1], &font_7x10, SSD1306_COLOR_WHITE);
    SSD1306_GotoXY(0, 22);
    SSD1306_Puts((char*)text_block[2], &font_7x10, SSD1306_COLOR_WHITE);
    SSD1306_GotoXY(0, 33);
    SSD1306_Puts((char*)text_block[3], &font_7x10, SSD1306_COLOR_WHITE);
    SSD1306_GotoXY(0, 44);
    SSD1306_Puts((char*)text_block[4], &font_7x10, SSD1306_COLOR_WHITE);
    SSD1306_UpdateScreen();
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C3_Init();
  MX_USB_HOST_Init();
  /* USER CODE BEGIN 2 */
  SSD1306_Init();

    SSD1306_Fill(SSD1306_COLOR_BLACK);
    SSD1306_GotoXY(0, 25);
    SSD1306_Puts("Init done", &font_7x10, SSD1306_COLOR_WHITE);
    SSD1306_UpdateScreen();
    HAL_Delay(1000);
    SSD1306_Fill(SSD1306_COLOR_BLACK);
    SSD1306_UpdateScreen();


  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */
    MX_USB_HOST_Process();

    if(err_flag == 1){
        SSD1306_Fill(SSD1306_COLOR_BLACK);
        SSD1306_GotoXY(0, 25);
        SSD1306_Puts("No slave connected!", &font_7x10, SSD1306_COLOR_WHITE);
        SSD1306_UpdateScreen();
        HAL_Delay(1000);
        SSD1306_Fill(SSD1306_COLOR_BLACK);
        continue;
    }


    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 7;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 8;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
